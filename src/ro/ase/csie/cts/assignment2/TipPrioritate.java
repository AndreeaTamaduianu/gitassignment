package ro.ase.csie.cts.assignment2;

public enum TipPrioritate {

	LOW, MEDIUM, HIGH;
}
