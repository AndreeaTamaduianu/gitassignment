package ro.ase.csie.cts.assignment2;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class TestGit {

	public static void main(String[] args) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Calendar calendar = Calendar.getInstance();
		String welcomeMessage="Hello Git! Denumirea proiectului de licență este\n"
				+ " \"Aplicatie pentru managementul tichetelor "
				+ "si al change-urilor pentru o firma ce ofera suport IT in regim 24/7\"";
		
		System.out.println(welcomeMessage);
		
		
		Tichet tk1=new Tichet(1, 100, "TK1", "desc1", TipSeveritate.CRITICAL, TipPrioritate.HIGH);
		Tichet tk2=new Tichet(2, 101,"TK2", "desc2", TipSeveritate.LOW,TipPrioritate.LOW);
		
		tk1.addComments("comment1sgg");
		tk2.addComments("comment1222");
		System.out.println("\n");

		System.out.println(tk1);
		System.out.println(tk2);
		
		
		try {
			tk1.closeTicket("comment2");
			tk2.closeTicket("comment3");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println(tk1);
		System.out.println(tk2);
		
		
		
		

	}

}
