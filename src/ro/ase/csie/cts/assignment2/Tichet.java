package ro.ase.csie.cts.assignment2;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

public class Tichet implements CloseResolveTicket {
		private int id;
		private int clientId;
		private String title;
		private String description;
		private TipSeveritate severity;
		private TipPrioritate priority;
		private String comments;
		private Date dateAdded;
		private Date dateClosed;
		private TipStare state;
		
		
	
	
		
		
		public Tichet(int id, int clientId, String title, String description, TipSeveritate severity,
				TipPrioritate priority) {
			
			this.id = id;
			this.clientId = clientId;
			this.title = title;
			this.description = description;
			this.severity = severity;
			this.priority = priority;
			this.state=TipStare.OPEN;
			this.dateAdded=Calendar.getInstance().getTime();
		}


		public Tichet(int id, int clientId, String title, String description, TipSeveritate severity,
				TipPrioritate priority, String comments, Date dateAdded, Date dateClosed, TipStare state) {
			
			this.id = id;
			this.clientId = clientId;
			this.title = title;
			this.description = description;
			this.severity = severity;
			this.priority = priority;
			this.comments = comments;
			this.dateAdded = dateAdded;
			this.dateClosed = dateClosed;
			this.state = state;
		}


		public String getDescription() {
			return description;
		}


		public void setDescription(String description) {
			this.description = description;
		}


		public String getComments() {
			return comments;
		}


		public Date getDateAdded() {
			return dateAdded;
		}


		public void setDateAdded(Date dateAdded) {
			this.dateAdded = dateAdded;
		}


		public Date getDateClosed() {
			return dateClosed;
		}


		public void setDateClosed(Date dateClosed) {
			this.dateClosed = dateClosed;
		}


		public int getId() {
			return id;
		}


		public int getClientId() {
			return clientId;
		}


		public String getTitle() {
			return title;
		}


		public TipSeveritate getSeverity() {
			return severity;
		}


		public TipPrioritate getPriority() {
			return priority;
		}


		public TipStare getState() {
			return state;
		}

		public void setState(TipStare state) {
			this.state = state;
		}
		
		
		

		@Override
		public String toString() {
			StringBuilder stringBuilder = new StringBuilder(); 
			stringBuilder.append("Tichet [id=");
			stringBuilder.append(id);
			stringBuilder.append(", clientId=");
			stringBuilder.append(clientId);
			stringBuilder.append(", title=");
			stringBuilder.append(title);
			stringBuilder.append(", description=");
			stringBuilder.append(description);
			stringBuilder.append(", severity=");
			stringBuilder.append(severity);
			stringBuilder.append(", priority=");
			stringBuilder.append(priority);
			stringBuilder.append(", comments=");
			stringBuilder.append(comments);
			stringBuilder.append(", dateAdded=");
			stringBuilder.append(dateAdded);
			stringBuilder.append(", dateClosed=");
			stringBuilder.append(dateClosed);
			stringBuilder.append( ", state=");
			stringBuilder.append(state);
			
			return stringBuilder.toString();  
		}


		@Override
		public void closeTicket(String comments) throws ParseException {
			StringBuilder stringBuilder = new StringBuilder(); 
			stringBuilder.append(this.comments+"\n"+comments);
			this.comments=stringBuilder.toString();
			
			this.dateClosed=Calendar.getInstance().getTime();
			this.state=TipStare.CLOSED;
			
		}
		
		public void addComments(String comments) {
			if(this.comments!=null) {
				StringBuilder stringBuilder = new StringBuilder(); 
				stringBuilder.append(this.comments+"\n"+comments);
				this.comments=stringBuilder.toString();
			}else {
				this.comments=comments;
			}
			
			
			
		}



	
		
		
		
		
		
		
		
	
}
