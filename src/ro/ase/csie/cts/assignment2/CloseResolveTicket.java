package ro.ase.csie.cts.assignment2;

import java.text.ParseException;
import java.util.Date;

public interface CloseResolveTicket {

	public void closeTicket(String comments) throws ParseException;
}
